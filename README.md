# Microsoft Office Partners Project

## TL;DR

### GIT
```
cd <project_dir>
git clone git@git.assembla.com:orba-microsoft-office-partners-internal.git microsoft
cd microsoft
```

### Virtualbox
The official recommended Virtualbox version is 5.1.14 or higher.
Check your version:
```
vboxmanage --version
```

### VAGRANT
Minimum required Vagrant version is 1.9.2 or higher.
Check your vagrant version:
```
vagrant -v
```

### NFS
Required NFS on local env. Install NFS by:
```
sudo apt-get install nfs-kernel-server nfs-common
```

### Ansible
Required ansible 2.3
Check your ansible version:
```
ansible --version
apt-get install -y software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt-get update --quiet
apt-get install -y -qq python ansible cowsay
```

### RUN DEV ENV
```
cd <project_dir>/microsoft/vagrant/virtual-dev
vagrant up
```

### Hosts on local
As root add lines to local /etc/hosts
```
# Microsoft - vagrant - virtual-dev
192.168.88.22 microsoft.dev
# Microsoft - vagrant - virtual-uat
192.168.88.23 001.microsoft.uat
192.168.88.23 002.microsoft.uat
192.168.88.23 003.microsoft.uat
192.168.88.23 004.microsoft.uat
192.168.88.23 005.microsoft.uat
```

### Ansible provision and install virtual-dev

Plugin M2 Payu.pl is required for proper full app installation.
Before installation check that you have access to orba-magento-plugins to avoid problems.

How to install? Read environment/readme.md

### SSH to Vagrant virtual-dev
```
cd <project_dir>/microsoft/vagrant/virtual-dev/
vagrant ssh # or ssh ubuntu@192.168.88.22
cd /cd /var/www/virtual-dev/
```

## Vault

### Creating Encrypted Files
`ansible-vault create foo.yml`

### Editing Encrypted Files
`ansible-vault edit foo.yml`

### Rekeying Encrypted Files
`ansible-vault rekey foo.yml`

### Encrypting Unencrypted Files
`ansible-vault encrypt foo.yml`

### Decrypting Encrypted Files
`ansible-vault decrypt foo.yml`

### Viewing Encrypted Files
`ansible-vault view foo.yml`

### Running a Playbook With Vault
`ansible-playbook site.yml --ask-vault-pass`

or

`ansible-playbook site.yml --vault-password-file ~/.vault_pass.txt`


## 1. GIT

### 1.1. Branches

There should be two main branches:

* master - current stable code
* uat - user acceptance testing branch

Each feature (ticket) should have its own branch with naming convention as follows:

* feature/ID-short-name - for new features
* bug/ID-short-name - for bug fixes
* hotfix/ID-short-name - for critical hotfixes that are deployed directly to production

Where ID is Assembla ticket ID and short-name is a 1-4 words description of task, eg.

* feature/123-password-validation
* bug/156-importer-process
* hotfix/178-checkout-button

Each release should have its own branch with naming convention as follows:
 
* release-A.B.C.D

Where:
 
* A - major (should be 0 during development, 1 after going public, 2 if there are drastic changes to the whole system in future, etc.)
* B - minor (should be incremented if there is any new feature in the release)
* C - revision (should be incremented if there were no new features in the release)
* D - patch (should be incremented if the release is a critical hotfix, it must be omitted if equals 0)

Examples:

release-0.1.0 > release-0.1.1 > release-0.1.2 > release-0.1.2.1 > release-0.1.3 > release-0.2.0 > release-0.2.1 > release-0.2.2 > release-0.2.2.1 > release-0.3.0 > release-0.3.1 > release-1.0.0

Feature and release branches should be created always from master branch.

### 1.2. Merges

The flow of merging branches should look like that:

* feature/bug/hotfix > uat (merge request by any author, merge by leader)
* feature/bug > release (manual merge by leader)
* release > master (manual merge by leader)
* hotfix > master (manual merge by leader)
* master > feature/bug (manual merge by leader)

There is a lot of merging here but it's needed due to client expectations (eg. removing some feature just before releasing it, not testing feature for a long time, etc.).

### 1.3. Tags

Tag should be created for each release after merging it to master. The name of tag should be the version number of release.

### 1.4. Commits

Each commit message (except merges) should be prefixed with "Re #ID " where ID is internal Assembla ticket ID. Then, small description is needed. 

## 2. Assembla

### 2.1. Tickets

There are two Assembla projects. One business and one internal. Business tickets are created by client. Internal tickets are created by developers. If an internal ticket is related to business one, its name should be suffixed with #ID, where ID is an ID of business ticket.

### 2.2. Milestones

Each sprint / release should have its own milestone both in business and internal project. Release milestone should be named "Release X", where X is release version, eg. "Release 1.2.0.1".

### 2.3. Statuses flow

On internal project:

* New > In progress (switched by developer when getting the ticket)
* In progress > Internal Test (switched by developer when completing work on ticket and creating merge request, description with test cases required, attach ticket to tester)
* Internal Test > Test Failed (switched by leader/tester if it doesn't work, description in ticket or code comment required, attach ticket to author)
* Internal Test > UAT (switched by leader after successful internal test)
* UAT > Test Failed (switched by leader, description in ticket comment required, attach ticket to author)
* Test Failed > In progress (switched by developer when getting back to ticket)
* UAT > Ready to release (switched by leader after successful UAT)
* Ready to release > Released (switched by leader after release)
* Released > Fixed (switched by leader after client test on production)
* Any > Invalid (switched by leader, description needed)

### 2.4. Code review

Code review should be performed by leader for big tasks or another developer for smaller tasks. We're using Assembla merge requests feature to perform code review.

## 3. Code

### 3.1. First steps

#### 3.2. Config GIT

Before cloning repository, make sure you configured GIT for work with Assembla:

See: https://app.assembla.com/spaces/orba-microsoft-office-partners-internal/git/instructions

Also, especially if you're working on Windows host:

    git config --global core.autocrlf false

#### 3.3. Clone repository

    git clone git@git.assembla.com:orba-microsoft-office-partners-internal.git microsoft

### 3.4. New features

Each feature should have its own Magento module. Files of the module should be placed in subfolder of one of the corresponding folders.

### 3.5. GIT

After GIT pull/checkout/merge, etc. developer should run:

    composer install
    
It will install/remove all dependencies.
    
Developer should commit `composer.lock` file if it was changed.

## 4. Misc

### 4.1. Translations

Developer should add every text to translations files.
    
## 5. Required server libs

### 5.1. Composer

PHP dependencies manager

See: https://getcomposer.org/download/

## 8. Appendix

Whenever you're changing content of this file, inform the whole team about it via e-mail or Assembla message. Explain the purpose of change, point to GIT branch in which your changes were made and write section numbers that were changed.

Whenever you're making a significant change or remove something, please use the following formatting: `[deleted]<s>Deleted text</s>[/deleted]`.
